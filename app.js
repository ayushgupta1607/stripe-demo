import express from "express";
import stripeInitializer from "stripe";

const stripe = stripeInitializer(
  "sk_test_51Iy9gaSGJOXeO7cdtPnn1JjcoWwPG61dCOkCOWe5McpdwbzSJqk4JwsN4AF39nRC4gJV17JsGb8y8UDB2skWjQXo00HmqLRYw7"
);
const app = express();
app.use(express.static("."));
app.post("/createcheckoutsession", async (req, res) => {
  const session = await stripe.checkout.sessions.create({
    payment_method_types: ["card"],
    line_items: [
      {
        price_data: {
          currency: "usd",
          product_data: {
            name: "Stubborn Attachments",
          },
          unit_amount: 2000,
        },
        quantity: 1,
      },
    ],
    mode: "payment",
    success_url: `https://www.example.com/success`,
    cancel_url: `https://www.example.com/cancel`,
  });
  res.json({ id: session.id });
});

app.listen(1312, console.log("app runing on 1312"));
